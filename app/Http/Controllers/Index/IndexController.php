<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Services\DatabaseService;
use App\Services\DatabaseServiceInterface;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var DatabaseServiceInterface|DatabaseService
     */
    private $service;

    /**
     * @var array
     */
    private $data = [];

    /**
     * Create a new authentication controller instance.
     * @param DatabaseServiceInterface $service
     */
    public function __construct(DatabaseServiceInterface $service)
    {
        $this->service = $service;
    }

    public function getMain(Request $request, $token = null)
    {
        $this->getDatabases();

        return view('index', $this->data);
    }

    public function getDatabaseInfo(Request $request, $token = null)
    {
        $this->getDatabases();
        $this->getTables($request->route('database'));

        return view('index', $this->data);
    }

    protected function getDatabases()
    {
        $this->data['databases'] = $this->service->getDatabases();
    }

    protected function getTables($database)
    {
        $this->data['tables'] = $this->service->getTables($database);
    }
}
