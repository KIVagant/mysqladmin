<?php

Route::get('/', 'Index\IndexController@getMain')->name('main');
Route::get('/database/{database}', 'Index\IndexController@getDatabaseInfo')->name('database_info');
