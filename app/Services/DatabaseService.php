<?php
namespace App\Services;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\ConnectionResolverInterface;

class DatabaseService implements DatabaseServiceInterface
{
    const MYSQL_DB_NAME = 'mysql';
    const MYSQL_DB_NAME_REPLACE = 'mysql_default_database';
    /**
     * @var Application
     */
    private $application;
    /**
     * @var ConnectionResolverInterface
     */
    private $connection;

    public function __construct(
        Application $application,
        ConnectionResolverInterface $connection
    )
    {
        $this->application = $application;
        $this->connection = $connection;
    }

    public function getTables($database)
    {
        if ($database === self::MYSQL_DB_NAME) {
            $database = self::MYSQL_DB_NAME_REPLACE;
        }
        $this->configureConnectionByName($database);
        $connection = $this->connection->connection($database);

        $result = $connection->select('SHOW TABLES');
        $result = array_map(function($item) use ($database) {
            $databaseName = $database === self::MYSQL_DB_NAME_REPLACE
                ? self::MYSQL_DB_NAME
                : $database;

            return $item->{'Tables_in_' . $databaseName};
        }, $result);

        return $result;
    }

    public function getDatabases()
    {
        return $this->connection->select('SHOW DATABASES');
    }

    /**
     * Configures a database connection on the fly
     * @param  string $databaseName The database name.
     * @return void
     */
    protected function configureConnectionByName($databaseName = null)
    {
        if (!$databaseName) {

            return false;
        }
        $config = $this->application->make('config');
        $connections = $config->get('database.connections');
        $newConnection = $connections[$config->get('database.default')];
        $newConnection['database'] = $databaseName === self::MYSQL_DB_NAME_REPLACE
            ? self::MYSQL_DB_NAME
            : $databaseName;
        // This will add our new connection to the run-time configuration for the duration of the request
        $this->application->make('config')->set('database.connections.' . $databaseName, $newConnection);

        return $this->application->make('config')->get('database.connections.' . $databaseName);
    }
}