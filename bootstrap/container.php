<?php

$app->bind(\App\Services\DatabaseServiceInterface::class, \App\Services\DatabaseService::class);
$app->bind(\Illuminate\Database\ConnectionResolverInterface::class, \Illuminate\Database\DatabaseManager::class);
