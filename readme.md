# The task

Write an mysql admin prototype for two hours

# Installation

1. Create the database for project settings. Yep, it's strange for the mysql admin-panel,
   but it's a default framework requirements.
2. Edit .env file and setup a mysql connection with the full access to any databases (root)
3. Run ```php artisan key:generate```
4. Run ```php artisan migrate --env=local```
5. Run ```php artisan serve```
6. Open http://localhost:8000/

# The Result

The only two routes now is available:

- / – databases list for current connection
- /database/[:db_name] – tables list inside a database