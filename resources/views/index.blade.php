@extends('layouts.common')
@section('header')
    Mysql admin panel
@stop

@section('menu_one')
    @foreach ($databases as $database)
        <li ><a href="/database/{{ $database->Database }}">{{ $database->Database }}</a></li>
        {{--<li class="active"><a href="#">{{ $database->Database }} <span class="sr-only">(current)</span></a></li>--}}
    @endforeach
@stop

@section('tables_list')
    @if(isset($tables) && $tables)
    <h2 class="sub-header">Таблицы</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Имя</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($tables as $table)
                <tr>
                    <td><a href="/database/table/{{ $table }}">{{ $table }}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @else
        <h2 class="sub-header">Базы данных:</h2>
        <div class="table-responsive">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($databases as $database)
                        <tr>
                            <td><a href="/database/{{ $database->Database }}">{{ $database->Database }}</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    @endif
@stop